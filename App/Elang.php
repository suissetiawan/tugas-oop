<?php 
class Elang{
	use Hewan,Fight;

	public function __construct($name){
		$this->name = $name;
		$this->jumlahKaki = 2;
		$this->keahlian = "terbang tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
	}

	public function getInfoHewan(){
		$str = "Nama hewan   : {$this->name}<br>
				Jenis hewan  : Elang<br>
				Jumlah Kaki  : {$this->jumlahKaki}<br>
				keahlian	 : {$this->keahlian}<br>
				Darah 		 : {$this->darah}<br>
				Attack Power : {$this->attackPower}<br>
				Defence Power: {$this->defencePower}<br>
				Atraksi		 : {$this->atraksi()}";
		echo $str; 
	}
}