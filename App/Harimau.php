<?php 
class Harimau{
	use Hewan,Fight;

	public function __construct($name){
		$this->name = $name;
		$this->jumlahKaki = 4;
		$this->keahlian = "Lari cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
	}

	public function getInfoHewan(){
		$str = "Nama hewan   : {$this->name}<br>
				Jenis hewan  : Harimau<br>
				jumlah Kaki  : {$this->jumlahKaki}<br>
				keahlian	 : {$this->keahlian}<br>
				Darah 		 : {$this->darah}<br>
				Attack Power : {$this->attackPower}<br>
				Defence Power: {$this->defencePower}<br>
				Atraksi		 : {$this->atraksi()}";
		echo $str; 
	}
}