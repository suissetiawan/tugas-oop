<?php 

trait Hewan{
	public $name,
		   $darah = 50,
		   $jumlahKaki,
		   $keahlian;

	public function atraksi(){
		$str = "{$this->name} sedang {$this->keahlian}";
		return $str;
	}

	abstract public function getInfoHewan();
}