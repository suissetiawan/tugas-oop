<?php 
require_once 'App/Hewan.php';
require_once 'App/Fight.php';
require_once 'App/Harimau.php';
require_once 'App/Elang.php';

//-----Instance Class-----------
$elang1 = new Elang("Eagle Eye");
$harimau1 = new Harimau("Tiger Zero");


//--------Cetak---------
echo "<h2><b>Info Hewan</b></h2>";
$elang1->getInfoHewan();
echo "<br> <br>";
$harimau1->getInfoHewan();
echo "<hr>";

echo "<h2><b>Game Dimulai</b></h2>";
$elang1->serang($elang1);
echo "<br> <br>";
$harimau1->diserang($elang1);